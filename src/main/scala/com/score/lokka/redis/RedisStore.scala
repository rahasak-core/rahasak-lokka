package com.score.lokka.redis

import com.score.lokka.config.RedisConf
import com.score.lokka.util.AppLogger
import redis.clients.jedis.Jedis

import scala.collection.JavaConverters._
import scala.collection.mutable

object RedisStore extends RedisCluster with AppLogger with RedisConf {

  lazy val redisStore = new Jedis(redisHost, redisPort)

  def get: mutable.Set[String] = {
    val jedis = redisPool.getResource
    val ids = jedis.smembers(redisKey).asScala
    redisPool.returnResource(jedis)

    logger.info(s"get trans ids in redis $ids")

    ids
  }

  def rm(ids: List[String]): Long = {
    val jedis = redisPool.getResource
    val arr = ids.toArray
    val i = jedis.srem(redisKey, arr: _*)
    redisPool.returnResource(jedis)

    logger.info(s"remove trans ids redis $ids")

    i
  }

}
