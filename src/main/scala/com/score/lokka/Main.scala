package com.score.lokka

import akka.actor.ActorSystem
import com.score.lokka.cassandra.CassandraStore
import com.score.lokka.config.AppConf
import com.score.lokka.elastic.ElasticStore
import com.score.lokka.stream.Streamer
import com.score.lokka.util.{LoggerFactory, CryptoFactory}

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // actor system mystiko
  implicit val system = ActorSystem.create("rahasak")

  // set up keys
  CryptoFactory.init()

  // create schema/indexes
  CassandraStore.init()
  ElasticStore.init()

  // init streamer
  Streamer.init()

}
